var express = require("express");
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

// express 部份
app.get('/', function(req, res){
    // res.send({ hello: "world" });
    res.send();
//   res.sendFile(__dirname + '/index.html');
});

// // express 套用資料
// app.use('/js', express.static('js'));
// app.use('/css', express.static('css'));
// app.use('/images', express.static('images'));


// 顯示port  
http.listen(port, function(){
    console.log('listening on *:' + port);
});

io.on('connection', function(socket) {
    socket.on('message', function (data) {
        io.sockets.emit('message', data)
        // NOTE 向所有人發送訊息，除發送者外
        // socket.broadcast.emit('message', data);
    });
});
